﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_ECattempt1
{
    //follows this tutorial https://www.c-sharpcorner.com/article/how-to-get-time-using-clocks-in-xamarin-android-app-using-visual-stu/
    //and references https://www.youtube.com/watch?v=eDx8tbUrkP0
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Alarm : ContentPage
    {
        public Alarm()
        {
            //shows the clock starting from the seconds
            InitializeComponent();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                //makes it so that it shows the hours, minutes and seconds (in military time)
                Device.BeginInvokeOnMainThread(() =>
                Clocklabel.Text = DateTime.Now.ToString("HH:mm:ss")

                );
                return true;
            });
        }
    }
}