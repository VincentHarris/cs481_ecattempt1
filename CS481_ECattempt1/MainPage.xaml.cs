﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_ECattempt1
{
    //follows this reference https://www.c-sharpcorner.com/article/create-stopwatch-application-in-xamarin-forms-using-visual-studio/
    //and also this https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.stopwatch?view=netcore-3.1
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        Stopwatch stpwtch;
        public MainPage()
        {
            InitializeComponent();
            stpwtch = new Stopwatch(); //stopwatch is written as stpwtch to avoid ambiguities
            stopwatchlabel.Text = "00:00:00.00000"; //this forces the stopwatch to have hours, minutes, seconds and milliseconds format
        }

        private void startbutton_Clicked(object sender, EventArgs e)
        {
            if (!stpwtch.IsRunning)
            {
                stpwtch.Start();
                //This makes it so that is shows the timer starting from milliseconds, which eventually turns into seconds, then minutes and so on
                Device.StartTimer(TimeSpan.FromMilliseconds(100), () =>
                {
                    //This line makes the stopwatchlabel in the xaml file count with the devices actual timer and displays it as a string 
                    stopwatchlabel.Text = stpwtch.Elapsed.ToString();

                    //This is needed so that is doesn't throw this error:
                    //Error CS1643  Not all code paths return a value in lambda expression of type 'Func<bool>'
                    if (!stpwtch.IsRunning) {  return false; }
                  else { return true; }
                });
            }
        }

        //stops the stopwatch
        private void stopbutton_Clicked(object sender, EventArgs e)
        {
            startbutton.Text = "Resume";
            stpwtch.Stop(); 
        }

        //erases the previous time and sets it back to 00:00:00.00000
        private void resetbutton_Clicked(object sender, EventArgs e)
        {
            startbutton.Text = "Start";
            stpwtch.Reset();
        }
    }
}
