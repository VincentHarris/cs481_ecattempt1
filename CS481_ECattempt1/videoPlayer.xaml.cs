﻿using LibVLCSharp.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_ECattempt1
{
    //references https://www.youtube.com/watch?v=uYaakilLkkk&t=42s
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class videoPlayer : ContentPage
    {
        //uses the plug vlc plugin package
        LibVLC libvlc;

        public videoPlayer()
        {
            InitializeComponent();
            Core.Initialize();
            libvlc = new LibVLC();

            //takes the mp4 video from the website given and plays it
            var media = new Media(libvlc, "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4", FromType.FromLocation);
            videoplayer.MediaPlayer = new MediaPlayer(media)
            { EnableHardwareDecoding = true };
            videoplayer.MediaPlayer.Play();
        }
    }
}